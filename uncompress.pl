#!/usr/bin/env perl

use strict;
use warnings;

    binmode STDIN;
    
    my $buf;
    read STDIN, $buf, 1; my $indexbytes = unpack 'C', $buf;
    read STDIN, $buf, 4; my $dictsize = unpack 'L', $buf;
    read STDIN, $buf, $dictsize;
    my @dict = split /\0/, $buf;
    my @doc;
    my $prevspace = 1;
    while (read(STDIN, $buf, $indexbytes) > 0) {
        my $index = unpack $indexbytes == 1 ? 'C' : 'S', $buf;
        my $word = $dict[$index];
        my $space = scalar $word =~ /^\s+$/;
        push (@doc, ' ') if !$space && !$prevspace;
        push @doc, $word;
        $prevspace = $space;
    }
    print join '', @doc;
