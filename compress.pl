#!/usr/bin/env perl

use strict;
use warnings;

    undef $/;
    my $doc = <>;
    
    my @words = grep { $_ ne ' ' } split /(\s+)/s, $doc;
    my %dict = map { $_ => 1 } @words;
    my @dictkeys = keys %dict;
    die "dictionary too large" if $#dictkeys > 65535; 
    my $i = 0;
    my $dictsize = 0;
    map { $dict{$_} = $i++; $dictsize += length($_) + 1 } @dictkeys;
    my $indexbytes = $i > 255 ? 2 : 1;

    binmode STDOUT;
    
    print pack 'C', $indexbytes;
    print pack 'L', $dictsize;
    foreach my $k (@dictkeys) { print pack 'Z*', $k } # could pack chars into 6-bits
    foreach my $w (@words) { print pack $indexbytes == 1 ? 'C' : 'S', $dict{$w} }
